package nmu_soloviov.labrab2;
import java.util.Random;

/**
 * Created by Андрей on 13.11.2016.
 */
class matrix {
    private int [][] massA= new int[10][10];
    private int size;
    public matrix(){
        this.size=5;
        Random rand = new Random();
        for (int i = 0; i < (5); i++) {
            for (int j = 0; j < (5); j++) {
                massA[i][j] = rand.nextInt(50);
            }
        }
    }
    public matrix(int size){
        this.size=size;
        Random rand = new Random();
        for (int i = 0; i < (size); i++) {
            for (int j = 0; j < (size); j++) {
                massA[i][j] = rand.nextInt(50);
            }
        }
    }
    public void print(){
        for (int i = 0; i < (this.size); i++) {
            for (int j = 0; j < (this.size); j++) {
                    System.out.print(this.massA[i][j] + "\t");
            }
            System.out.println();
        }
    }
    public void findmin(){
        int minimum = 100;

        int countofmin=1;
        for (int i = 0; i < (this.size); i++) {
            for (int j = 0; j < (this.size); j++) {
                if (this.massA[i][j] == minimum) {
                    countofmin = countofmin + 1;
                }
                if (this.massA[i][j] < minimum) {
                    minimum = this.massA[i][j];
                    countofmin = 1;
                }
            }

        }
        System.out.println("Наименьшее число массива : " + minimum + ",кол-во минимумов "+countofmin );
    }
    }

