package nmu_soloviov.labrab1;

import java.util.Random;

/**
 * Created by Андрей on 01.10.2016.
 */
public class labrab1 {
    public static void main(String[] args) {
        System.out.println("Поиск максимального элемента в массиве");
        int[]mass= new int[10];
        Random rand= new Random();

        for(int i=0; i<mass.length;i++)
            mass[i]=rand.nextInt(100);
        for(int i=0; i<mass.length;i++)
            System.out.print(mass[i]+" ");
        int maxIndex = 0;
        for (int i = 0; i < mass.length; i++)
        {
            if (mass[maxIndex] < mass[i])
                maxIndex = i;}
        System.out.println(" Максимальный эллемент = " + mass[maxIndex]);
        System.out.println("Поиск максимального элемента в матрице");
        int[][] matrixA;
        matrixA = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrixA[i][j]=rand.nextInt(100);
            }
            System.out.println();
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
        int maxElement=0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (maxElement < matrixA[i][j])
                    maxElement = matrixA[i][j];
            }
        }
        System.out.println(" Максимальный эллемент = " + maxElement);

    }
}