package nmu_soloviov.labrab6;

/**
 * Created by Андрей on 06.12.2016.
 */
public class Console {
        String brand;
        String model;
        Integer price;
        Shop shop;


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Console console = (Console) o;

            if (!brand.equals(console.brand)) return false;
            if (!model.equals(console.model)) return false;
            if (!price.equals(console.price)) return false;
            return shop.equals(console.shop);

        }

        @Override
        public int hashCode() {
            int result = brand.hashCode();
            result = 31 * result + model.hashCode();
            result = 31 * result + price.hashCode();
            return result;
        }
    }

