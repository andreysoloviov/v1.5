package nmu_soloviov.labrab6;
import com.google.gson.Gson;
/**
 * Created by Андрей on 06.12.2016.
 */
public class lab6 {
    public static void main(String[] args) {
        Console console= new Console();
        console.brand="Sony";
        console.model="PS4";
        console.price= 350;
        Shop shop=new Shop();
        shop.name="Jam";
        shop.country="Ukraine";
        console.shop=shop;

        Gson gson=new Gson();
        String json=gson.toJson(console);
        System.out.println(json);
        Console studentFromJson= new Console();
        gson.fromJson(json,Console.class);
    }

}
