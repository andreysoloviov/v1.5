package nmu_soloviov.labrab6;

/**
 * Created by Андрей on 06.12.2016.
 */
public class Shop{
    String name;
    String country;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shop shop = (Shop) o;

        if (!name.equals(shop.name)) return false;
        return name.equals(shop.name);
    }

}
